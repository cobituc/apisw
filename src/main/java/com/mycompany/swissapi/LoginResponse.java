package com.mycompany.swissapi;

public class LoginResponse {

    private String token;
    private String exp;
    ModelEspecifico ModelEspecificoObject;
    private String seguridad = null;

    //encapsulo modeloespecifico
   

    // Getter Methods 
    public String getToken() {
        return token;
    }

    public String getExp() {
        return exp;
    }

    public ModelEspecifico getModelEspecifico() {
        return ModelEspecificoObject;
    }

    public String getSeguridad() {
        return seguridad;
    }

    // Setter Methods 
    public void setToken(String token) {
        this.token = token;
    }

    public void setExp(String exp) {
        this.exp = exp;
    }

    public void setModelEspecifico(ModelEspecifico modelEspecificoObject) {
        this.ModelEspecificoObject = modelEspecificoObject;
    }

    public void setSeguridad(String seguridad) {
        this.seguridad = seguridad;
    }
    
    
    public void mostrarRespuesta(){
        System.out.println("Token"+this.getToken());
        System.out.println("exp"+this.getExp());
        System.out.println("seguridad"+this.getSeguridad());
        System.out.println("----------------------------------------------------------------");
        System.out.println("CuitPrestador"+this.getModelEspecifico().getCuitPrestador());
        System.out.println("tipoPrestador"+this.getModelEspecifico().getTipoPrestador());
        System.out.println("codigoPrestador"+this.getModelEspecifico().getCodigoPrestador());
        System.out.println("tipoPrestadorDesc"+this.getModelEspecifico().getTipoPrestadorDesc());
        System.out.println("numeroLegajo"+this.getModelEspecifico().getNumeroLegajo());        
    }
}
