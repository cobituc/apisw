package com.mycompany.swissapi;

public class ModelEspecifico {

    private String cuitPrestador;
    private float codigoPrestador;
    private String tipoPrestadorDesc;
    private String tipoPrestador;
    private float numeroLegajo;

    // Getter Methods 
    public String getCuitPrestador() {
        return cuitPrestador;
    }

    public float getCodigoPrestador() {
        return codigoPrestador;
    }

    public String getTipoPrestadorDesc() {
        return tipoPrestadorDesc;
    }

    public String getTipoPrestador() {
        return tipoPrestador;
    }

    public float getNumeroLegajo() {
        return numeroLegajo;
    }

    // Setter Methods 
    public void setCuitPrestador(String cuitPrestador) {
        this.cuitPrestador = cuitPrestador;
    }

    public void setCodigoPrestador(float codigoPrestador) {
        this.codigoPrestador = codigoPrestador;
    }

    public void setTipoPrestadorDesc(String tipoPrestadorDesc) {
        this.tipoPrestadorDesc = tipoPrestadorDesc;
    }

    public void setTipoPrestador(String tipoPrestador) {
        this.tipoPrestador = tipoPrestador;
    }

    public void setNumeroLegajo(float numeroLegajo) {
        this.numeroLegajo = numeroLegajo;
    }

    public void mostrarPrestador() {
        System.out.println("CuitPrestador" + this.getCuitPrestador());
        System.out.println("tipoPrestador" + this.getTipoPrestador());
        System.out.println("codigoPrestador" + this.getCodigoPrestador());
        System.out.println("tipoPrestadorDesc" + this.getTipoPrestadorDesc());
        System.out.println("numeroLegajo" + this.getNumeroLegajo());
    }
}
