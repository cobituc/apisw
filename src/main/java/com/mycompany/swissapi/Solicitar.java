/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swissapi;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.Response;
import jdk.nashorn.internal.parser.JSONParser;

public class Solicitar {

    public static void main(String[] args) {
        //Esta variable res la usaremos únicamente para dar un respuesta final
        String resJson = "";
        String URL = "https://mobileint.swissmedical.com.ar/pre/api-smg/v0/auth-login";
        try {
            //Creamos el cliente de conexión al API Restful
            Client client = ClientBuilder.newClient();

            //Creamos el target lo cuál es nuestra URL junto con el nombre del método a llamar
            WebTarget target = client.target(URL);

            //Creamos nuestra solicitud que realizará el request
            Invocation.Builder solicitud = target.request();

            //Creamos y llenamos nuestro objeto BaseReq con los datos que solicita el API
            Login req = new Login();
            req.setApiKey("81ba7db467d68def9a81");
            req.setUsrLoginName("suap");
            req.setPassword("suap2018");
            req.setCuit("33677781489");
            Device dev = new Device();
            dev.setMessagingid("132H12312");
            dev.setDeviceid("192.168.45.77");
            dev.setDevicename("DELL-2Y0-DG");
            dev.setBloqueado(0);
            dev.setRecordar(0);
            req.setDevice(dev);

            //Convertimos el objeto req a un json
            Gson gson = new Gson();
            String jsonString = gson.toJson(req);
            System.out.println(jsonString);

            //Enviamos nuestro json vía post al API Restful
            Response post = solicitud.post(Entity.json(jsonString));

            //Recibimos la respuesta y la leemos en una clase de tipo String, en caso de que el json sea tipo json y no string, debemos usar la clase de tipo JsonObject.class en lugar de String.class
            String responseJson = post.readEntity(String.class);
            resJson = responseJson;

            //Imprimimos el status de la solicitud
            System.out.println("Estatus: " + post.getStatus());

            switch (post.getStatus()) {
                case 200:
                    resJson = responseJson;
                    break;
                default:
                    resJson = "Error";
                    break;
            }

//            try{
//            JsonElement element = JsonParser.parseString(resJson);
//            
//                com.google.gson.JsonObject objetoJson  = element.getAsJsonObject();
//                String modelo = objetoJson.get("").getAsString();
//            //JsonObject repsuesta = 
//        }catch (Exception e) {
//            //En caso de un error en la solicitud, llenaremos res con la exceptión para verificar que sucedió
//            resJson = e.toString();
//        }
//            
        } catch (Exception e) {
            //En caso de un error en la solicitud, llenaremos res con la exceptión para verificar que sucedió
            resJson = e.toString();
        }
        //Imprimimos la respuesta del API Restful
        System.out.println(resJson);

        Gson gson = new Gson();

        LoginResponse respuestaLogin = gson.fromJson(resJson, LoginResponse.class);

        System.out.println("Token=" + respuestaLogin.getToken());
        System.out.println("exp=" + respuestaLogin.getExp());
        System.out.println("seguridad=" + respuestaLogin.getSeguridad());
        System.out.println("----------------------------------------------------------------");
        System.out.println("CuitPrestador=" + respuestaLogin.getModelEspecifico().getCuitPrestador());
        System.out.println("tipoPrestador=" + respuestaLogin.getModelEspecifico().getTipoPrestador());
        System.out.println("codigoPrestador=" + respuestaLogin.getModelEspecifico().getCodigoPrestador());
        System.out.println("tipoPrestadorDesc=" + respuestaLogin.getModelEspecifico().getTipoPrestadorDesc());
        System.out.println("numeroLegajo=" + respuestaLogin.getModelEspecifico().getNumeroLegajo());
        
        
        
        
        
    }

}
